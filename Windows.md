## 常用应用

**本地使用的软件建议禁止联网**

下载站：[webnav](https://webnavigate.github.io/Software.html)	[filecroco](https://www.filecroco.com/)	[gndown](https://www.gndown.com/)

虚拟机： [ShadowDefender](http://shadowdefender.com/download.html)	key: 4UDMT-QBPGC-PYJ5N-BZWF8-B676R

浏览器：[Firefox](http://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=en-US)	

通讯：[钉钉](https://www.dingtalk.com/download)	[飞书](https://www.feishu.cn/download)	[微信](https://pc.weixin.qq.com/)/[企业](https://work.weixin.qq.com/#indexDownload)	[网易邮箱](https://dashi.163.com/index.html)	[ThunderBird](https://www.thunderbird.net/en-US/download/?downloaded=True&download_channel=esr)

文件：[7zip](https://www.7zip.com/)	[everything](https://www.voidtools.com/downloads)	[Notepad3](https://github.com/rizonesoft/Notepad3/releases)	[WPS](https://ai.wps.cn/)	[PDFpro](https://www.123pan.com/s/N7M7Vv-cjHod.html)	[Typro](https://download2.typoraio.cn/windows/typora-setup-x64.exe)/[patch](https://www.fahai.org/aDisk/Typora/x64_Patch.zip)	

媒体：[Potplayer](https://t1.daumcdn.net/potplayer/PotPlayer/Version/Latest/PotPlayerSetup64.exe)	[YTB](https://github.com/FreeTubeApp/FreeTube/releases/)	[OBS录屏](https://obsproject.com/)	[看图王](https://gndown.lanzoub.com/b047sodqd)	[PhotoMgr](https://github.com/tannerhelland/PhotoDemon)	[Xmind](https://xmind.app/zen/download/win64/)/[patch](https://www.fahai.org/aDisk/XMind/x64_Patch.zip)	

系统：[FireWall](https://github.com/henrypp/simplewall/releases)	[AutoRuns](https://download.sysinternals.com/files/Autoruns.zip)	[Context](https://github.com/BluePointLilac/ContextMenuManager/releases)	[DISM++](https://github.com/Chuyu-Team/Dism-Multi-language/releases)	[Driver](https://gndown.lanzoub.com/b0480360h)/[clean](https://github.com/lostindark/DriverStoreExplorer/releases)	[Upate](https://www.filecroco.com/download-windows-update-minitool/)	

工具：[AnLink](https://anl.ink/download/installer)/[scrcpy](https://github.com/Genymobile/scrcpy/releases)	[Filddler](https://www.telerik.com/fiddler/fiddler-classic)	[IDM](https://www.internetdownloadmanager.com/download.html)/[patch](https://cracksurl.com/internet-download-manager/)	

远程：[AnyDesk](https://download.anydesk.com/AnyDesk.exe)	[RustDesk](https://github.com/rustdesk/rustdesk/releases)	[向日葵](https://sunlogin.oray.com/download)

开发：[VScode](http://code.visualstudio.com/)	[git](https://github.com/git-for-windows/git/releases)	

Office 2016 pro plus：

SW_DVD5_Office_Professional_Plus_2016_64Bit_ChnSimp_MLF_X20-42426.ISO

> MAK：GTMNQ-K2777-RXXRB-TQ9MV-46YVM
>

插件：万彩办公大师	方方格子/Excel	iSlide/PPT	小恐龙排版助手/Word



## 系统重装

一、启动盘	

微PE：https://www.wepe.com.cn/

二、系统ISO

https://files.rg-adguard.net/	推荐企业版，ed2k 下载

Debloat Tool： [MSMG Toolkit](https://github.com/devbytemehedi/msmg-toolkit/releases)	[Tiny11](https://github.com/ntdevlabs/tiny11builder)

三、备份

软件：下载备份常用软件安装包到其他盘

文件：%userprofile%  %Appdata%	desktop	document	download

驱动：C:\Windows\System32\DriverStore\FileRepository

注册表：HKCU\software\，HKLM\software\policies\

四、重装

F8 -> USB启动 -> WinNTsetup，集成备份驱动和注册表，安装系统。

PE mount Registry：C:\Windows\System32\config



=> Activate

MAK Enterprise	XGVPP-NMH47-7TTHJ-W3FW7-8HV2C

=> Optimize

Highly recommend using registry  to optimize system: publicity, efficiency, editable, reusable, low-risk

referrences: https://www.elevenforum.com/
