## 常用应用

**本地使用的软件建议禁止联网**

下载站：[gndown](https://www.gndown.com/category/anzhuoruanjian/anzhuoyingyong)	[4fb](https://4fb.cn/soft/app/)	[backups](https://wwbi.lanzoub.com/b036sgz0j?pwd=4zrk)

浏览器：[Via](https://res.viayoo.com/v1/via-release.apk)	[mbrowser](http://mbrowser.mujiankeji.cn/m/t3.html)/[rss](https://gitlab.com/Elfingeek/apps/-/raw/main/mBrowser/rss.json)	[*Webview*](https://www.apkmirror.com/apk/google-inc/android-system-webview/)	

> [UA](https://whatmyuseragent.com/platforms): Mozilla/5.0 (BlackBerry) AppleWebKit/537.36 (KHTML, Like Gecko) Mobile Safari SogouSearch baiduboxapp baiduboxsenior

[输入法](https://www.coolapk.com/apk/tag/%E8%BE%93%E5%85%A5%E6%B3%95)：[讯飞mod](https://firepx.lanzoul.com/b00vf92jc?pwd=647w)	[搜狗mod](https://firepx.lanzoul.com/b00vp9m4h?pwd=3o0y)	[百度mod](https://firepx.lanzoul.com/b00vmbwli?pwd=3rwf)

地图：[百度mod](https://firepx.lanzoul.com/b00vyinhe?pwd=e2nb)	[腾讯mod](https://firepx.lanzoul.com/b00vzogna?pwd=1fzq)	[高德mod](https://firepx.lanzoul.com/b00vwatdg?pwd=bp1q)

网络：[WiFi钥匙](https://4fb.lanzoui.com/b01i202wb?pwd=4qik)	[Adguard](https://4fb.lanzoui.com/b01igve9a)	[IDM+](https://4fb.lanzoub.com/b01jx9h9e)	[网盘](https://423down.lanzouo.com/b0f1fl38j)

通讯：[钉钉](https://www.coolapk.com/apk/com.alibaba.android.rimet)	[飞书](https://www.coolapk.com/apk/com.ss.android.lark)	[微信](https://www.coolapk.com/apk/com.tencent.mm)	[TIM](https://www.coolapk.com/apk/com.tencent.tim)	[Mail](https://apk.support/download-app/com.fsck.k9)	

媒体：[Bilibili](https://423down.lanzouh.com/b0f1gksne)	[MxPlayer](https://pan.lanzoub.com/b0ai135ud)	

扫描：[布丁](https://www.budingscan.com/)	[坚果云](https://www.coolapk.com/apk/nutstore.android.scanner)	[证件照](https://4fb.lanzoub.com/b01kg55kd)	

文档：[Reader](https://4fb.lanzouw.com/b01ivx7yf)	[Note](http://www.j9p.com/azrj/544319.html)	[PDF](https://www.123pan.com/s/N7M7Vv-06Qod.html)	[WPS](https://mo.wps.cn/pc-app/office-pro.html)	

> WPS: R8R8P-MTT6F-KLRPM-J7CAB-PJM8C

学习：[*欧路词典*](https://apk.support/download-app/com.qianyan.eudic)/[离线词库](https://forum.freemdict.com/latest)	[小站雅思](https://coolapk.com/apk/com.zhan.ieltstiku)	[*DeepL*](https://apk.support/download-app/com.deepl.mobiletranslator)	[*Papago*](https://apk.support/download-app/com.naver.labs.translator)	

[购物](https://www.coolapk.com/apk/tag/%E8%B4%AD%E7%89%A9)：[淘宝](https://apk.support/download-app/com.taobao.taobao)	[拼多多](https://apk.support/download-app/com.xunmeng.pinduoduo)	[京东](https://apk.support/download-app/com.jingdong.app.mall)	[支付宝](https://apk.support/download-app/com.eg.android.AlipayGphone)	[闲鱼](https://coolapk.com/apk/com.taobao.idlefish)

[求职](https://www.coolapk.com/apk/tag/%E6%B1%82%E8%81%8C)：[Boss](https://www.coolapk.com/apk/com.hpbr.bosszhipin)	[拉勾](https://www.coolapk.com/apk/com.alpha.lagouapk)	[智联](https://coolapk.com/apk/com.zhaopin.social)	[前程](https://coolapk.com/apk/com.job.android)

[系统](https://423down.lanzouo.com/b0f1944od)：[FileMgr](https://www.coolapk.com/apk/bin.mt.plus)	[雪豹速清](https://www.coolapk.com/apk/com.idaodan.clean.master)	[ActivityMgr](https://github.com/sdex/ActivityManager/releases)	[MultiTTS](https://www.123pan.com/s/4UKiVv-6rgfh.html)	[Xposed](https://virtualxposed.org/download/)	

企查：[企查查](https://www.coolapk.com/apk/com.android.icredit)	[天眼查](https://www.coolapk.com/apk/com.tianyancha.skyeye)	[爱企查](https://www.coolapk.com/apk/com.baidu.xin.aiqicha)	[启信宝](https://www.coolapk.com/apk/com.bertadata.qxb)	

## 系统优化

1、骚扰拦截：拦截垃圾短信和骚扰电话

> 电话前缀：00,400,162,165,167,170,171,950,951,952,957
> 关键词：订阅,退订,取关,拒收,回复T,回T,恭喜,领取,优惠,福利,热线,号召,积分

2、权限管理：关闭敏感权限，减少隐私泄露

> ID/通讯录/短信/相册/定位/剪贴板/usage access）

3、通知管理：关闭无用通知，减少推送骚扰

4、应用设置：修改默认设置，关闭广告推送

5、省电优化：减少常驻后台，延长电池续航

6、联网控制：本地软件禁止联网 (SIM/WiFi/background)

7、开发者模式：关闭日志；降低动画时长加快响应；限制读取电话和短信

### Apps Management

#### NonROOT(ADB)

[platform tools](https://dl.google.com/android/repository/platform-tools-latest-windows.zip):

	adb devices
	adb shell pm uninstall --user 0 PackageName
	adb shell pm disable-user --user 0 PackageName
	adb shell pm enable PackageName

[Shizuku](https://github.com/RikkaApps/Shizuku/releases):

```
adb shell sh /sdcard/Android/data/moe.shizuku.privileged.api/start.sh
```

#### ROOT

Disable Ads Activity: [Blocker](https://github.com/lihenggui/blocker/releases)	[MyAndroidTools](https://github.com/wangqi060934/MyAndroidToolsWebsite/releases/)	
